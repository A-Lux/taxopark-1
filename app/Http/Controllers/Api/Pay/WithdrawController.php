<?php

namespace App\Http\Controllers\Api\Pay;

use App\Http\Controllers\Controller;
use App\Models\Interval;
use App\Models\Order;
use App\Models\UserCard;
use App\Paybox\Paybox;
use Carbon\Carbon;
use Carbon\Traits\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class WithdrawController extends Controller
{
    //

    public function withdraw(Request $request)
    {

        $user = $request->user();
        $valData = $request->validate([
            'user_card_id' => 'required',
            'price' => 'required'
        ]);
        $i = 0;
        $exception = 0;

        while ($exception  < 1){
            $park = new \stdClass();
            $park->id = $request->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
            $phone = new \stdClass();
            $phone->phones = [
                $user['phone']
            ];
            $park->driver_profile = $phone;
            $query = new \stdClass();
            $query->park = $park;

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'X-Client-ID' =>$request->delivery ? 'taxi/park/1bec42866c27423fb612b05e18f0c666' : 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                'X-Api-Key' =>$request->delivery ? 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX' : 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                'Content-Type' => 'application/json',
                'Accept-Language' => 'ru'
            ])->post('https://fleet-api.taxi.yandex.net/v1/parks/driver-profiles/list', [
                'query' => $query,
                'offset'  => 1000 * $i
            ]);
            $response = json_decode($response);
            $yandex_profile = '';
//            return response(['message' => $response]);


            foreach ($response->driver_profiles as $profile) {

                if (in_array($user['phone'], $profile->driver_profile->phones)) {
                    $yandex_profile = $profile->driver_profile;
                    $yandex_account = $profile->accounts[0];
                    if ($yandex_profile->work_rule_id == '4045e1cdbdb14ee1975c405480ddb332'
                        || $yandex_profile->work_rule_id == 'ee222adb75bc4d77a38682bc901d84d9'
                        || $yandex_profile->work_rule_id == '7350f01b3f3048cdb23606b366213650'
                        || $yandex_profile->work_rule_id == '603c7c520b0c4667befc9447d2cf4b9f'
                    ) {
                        return response(['message' => 'Ваш номер телефона не соответсвтует условиям'],200);
                    }
                    $user->update(['balance'=>$yandex_account->balance]);

                    $exception = 1;
                    break;
                }
            }

            $i += 1;
        }


        $interval = Interval::where('start_time','<=',Carbon::now()->toTimeString())
            ->where('end_time','>=',Carbon::now()->toTimeString())
            ->first();

        if ($interval){
            return response(['message' => 'С '.$interval->start_time.'-'.$interval->end_time.' вывод недоступен'],200);
        }


        if (setting('site.block_withdraw') == 1){
            return  response(['message' => 'Извините технические неполадки. Просьба связаться с тех.поддержкой'],423);
        }



        if ($request->user()->is_blocked == 1){
            return response(['message'=>'Извините технические неполадки. Просьба связаться с тех.поддержкой'],423);
        }
        if($valData['price'] < 5000){
            if ($user['balance']-($valData['price']+300) <500){
                if(($user['balance']-800) > 0)
                    return response(['message' => 'Вы можете снять только '.($user['balance']-800).'тг'],200);
                return response(['message' => 'Вы можете снять только 0 тг'],200);
            }
        }else{
            if ($user['balance']-($valData['price']) <500){
                if(($user['balance']-500) > 0)
                    return response(['message' => 'Вы можете снять только '.($user['balance']-500).'тг'],200);
                return response(['message' => 'Вы можете снять только 0 тг'],200);
            }
        }




        $yesterday = Carbon::now();
        $today = Carbon::now();
        $withdrawsInDayPrice = Order::where('user_id',$request->user()->id)

            ->whereIn('payment_type_id',[3,2])
            ->where(function($query) use ($yesterday,$today){
                $query->where('created_at','>=',$yesterday->setTime(0,0,0));
                $query->where('created_at','<=',$today->setTime(23,59,59));
            })
            ->where('payment_status_id',4)
            ->sum('price');

        if (($withdrawsInDayPrice+$valData['price']) >10000){
            return response(['message' => 'Лимит на вывод 10000тг в сутки, если хотите снять больше звоните 87078408001'],200);
        }
        if ($user['balance'] >= $valData['price']+700){
            $userCard = UserCard::find($valData['user_card_id']);
            $order = Order::create([
                'user_id' => $user->id,
                'payment_type_id' => 2,
                'user_card_id' => $valData['user_card_id'],
                'payment_status_id' => 2,
                'price' => $valData['price'],
            ]);
            $query = [
                'pg_order_id' => (string) $order->id,
                'pg_amount' => $valData['price'],
                'pg_user_id' => $user->id,
                'pg_card_id_to' => $userCard->card_id,
                'pg_description' => 'Вывод номер '.$order->id,
                'pg_post_link' =>  $request->delivery ? route('withdrawDelivery') : route('withdrawStatus'),
                'pg_order_time_limit' => '2030-10-05 12:00:00',
                'pg_back_link' => route('login'),
                'pg_salt' => Str::random(10),
                'delivery' => $request->delivery

            ];
            $payment = new Paybox();
            $payment->setQuery($query);
            $response = $payment->withdraw();


            return response(['message' => $response],201);
        }else{
            return  response(['message' => 'Баланса недостаточно']);
        }
    }

    public function withoutCard(Request $request)
    {
        $user = $request->user();
        $valData = $request->validate([

            'price' => 'required|numeric'
        ]);
        $i = 0;
        $exception = 0;

        while ($exception  < 1){
            $park = new \stdClass();
            $park->id = $request->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
            $phone = new \stdClass();
            $phone->phones = [
                $user['phone']
            ];
            $park->driver_profile = $phone;
            $query = new \stdClass();
            $query->park = $park;

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'X-Client-ID' => $request->delivery ? 'taxi/park/1bec42866c27423fb612b05e18f0c666' : 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                'X-Api-Key' =>$request->delivery ? 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX' : 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                'Content-Type' => 'application/json',
                'Accept-Language' => 'ru'
            ])->post('https://fleet-api.taxi.yandex.net/v1/parks/driver-profiles/list', [
                'query' => $query,
                'offset'  => 1000 * $i
            ]);
            $response = json_decode($response);
            $yandex_profile = '';
//            return response(['message' => $response]);


            foreach ($response->driver_profiles as $profile) {

                if (in_array($user['phone'], $profile->driver_profile->phones)) {
                    $yandex_profile = $profile->driver_profile;
                    $yandex_account = $profile->accounts[0];
                    if ($yandex_profile->work_rule_id == '4045e1cdbdb14ee1975c405480ddb332'
                        || $yandex_profile->work_rule_id == 'ee222adb75bc4d77a38682bc901d84d9'
                        || $yandex_profile->work_rule_id == '7350f01b3f3048cdb23606b366213650'
                        || $yandex_profile->work_rule_id == '603c7c520b0c4667befc9447d2cf4b9f'
                    ) {
                        return response(['message' => 'Ваш номер телефона не соответсвтует условиям'],200);
                    }
                    $user->update(['balance'=>$yandex_account->balance]);

                    $exception = 1;
                    break;
                }
            }

            $i += 1;
        }


        $interval = Interval::where('start_time','<=',Carbon::now()->toTimeString())
            ->where('end_time','>=',Carbon::now()->toTimeString())
            ->first();


        if ($interval){
            return response(['message' => 'С '.$interval->start_time.'-'.$interval->end_time.' вывод недоступен'],200);
        }

        if (setting('site.block_withdraw') == 1){
            return  response(['message' => 'Извините технические неполадки. Просьба связаться с тех.поддержкой'],423);
        }



        if ($request->user()->is_blocked == 1){
            return response(['message'=>'Извините технические неполадки. Просьба связаться с тех.поддержкой'],423);
        }
        if($valData['price'] < 5000){
            if ($user['balance']-($valData['price']+300) <500){
                if(($user['balance']-800) > 0)
                    return response(['message' => 'Вы можете снять только '.($user['balance']-800).'тг'],200);
                return response(['message' => 'Вы можете снять только 0 тг'],200);
            }
        }else{
            if ($user['balance']-($valData['price']) <500){
                if(($user['balance']-500) > 0)
                    return response(['message' => 'Вы можете снять только '.($user['balance']-500).'тг'],200);
                return response(['message' => 'Вы можете снять только 0 тг'],200);
            }
        }
//        dd(CCarbon::now()->subDays(1));
        $yesterday = Carbon::now();
        $today = Carbon::now();
        $withdrawsInDayPrice = Order::where('user_id',$request->user()->id)

            ->whereIn('payment_type_id',[3,2])
            ->where(function($query) use ($yesterday,$today){
                $query->where('created_at','>=',$yesterday->setTime(0,0,0));
                $query->where('created_at','<=',$today->setTime(23,59,59));
            })
            ->where('payment_status_id',4)
            ->sum('price');
        if (($withdrawsInDayPrice+$valData['price']) >10000){
            return response(['message' => 'Лимит на вывод 10000тг в сутки, если хотите снять больше звоните 87078408001'],200);
        }


        if ($user['balance'] >= $valData['price']+700){


            $order = Order::create([
                'user_id' => $user->id,
                'payment_type_id' => 3,
                'payment_status_id' => 2,
                'user_card_id' => null,
                'price' => $valData['price']
            ]);
//            dd(Carbon::now()->addHours(6));

            $query = [
                'pg_order_id' => (string) $order->id,
                'pg_amount' => $valData['price'],
                'pg_description' => 'Вывод номер '.$order->id,
                'pg_post_link' => $request->delivery ? route('withdrawDelivery') : route('withdrawStatus'),
                'pg_back_link' => route('login'),
                'pg_order_time_limit' =>'2030-10-05 12:00:00',
                'pg_salt' => Str::random(10),
                'delivery' => $request->delivery


            ];

            $withdraw = new Paybox();
            $withdraw->setQuery($query);
            $response = $withdraw->withdrawWithoutCard();
            return response(['response' => $response],201);
        }else{
            return response(['message' => 'Не хватает баланса'],422);
        }


    }
}
