<?php

namespace App\Http\Controllers\Api\Pay;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class StatusController extends Controller
{
    //
    public function withdrawStatus(Request $request)
    {
        if (!$request->has('pg_xml')){
            return false;
        }

        Log::error('PG_XML '.$request->pg_xml);

        $pg_xml = $request->pg_xml;
        $xml = new \SimpleXMLElement($pg_xml);
        if ($xml->pg_status == 'ok'){
            try{
                $order = Order::find($xml->pg_order_id);

                if($order['payment_status_id'] == 4){
                    Log::error('PG_XML '.$request->pg_xml);
                    return response([],200);
                }
                $order['payment_status_id'] = 4;
                $order->save();
                $user = User::find($order['user_id']);
                $user['balance'] -= ($order->price >=5000 ? $order->price : $order['price']+300);
                $user->save();
                $park = new \stdClass();
                $park->id = $xml->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
                $query = new \stdClass();
                $query->amount ="-".($order->price >= 5000 ? $order->price : $order->price+300)."";
                $query->category_id = "partner_service_manual_9";
                $query->description = "Вывод номер ".$order->id. " .Номер :".$user->phone;
                $query->driver_profile_id =$user->driver_id;
                $query->park_id = $xml->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
                $query = (array) $query;
                $response = Http::withHeaders([
                    'Accept' => 'application/json',
                    'X-Client-ID' => $xml->delivery ? 'taxi/park/1bec42866c27423fb612b05e18f0c666' : 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                    'X-Api-Key' => $xml->delivery ? 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX' : 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                    'Content-Type' => 'application/json',
                    'Accept-Language' => 'ru',
                    'X-Idempotency-Token' => (string) Str::uuid()
                ])->post('https://fleet-api.taxi.yandex.net/v2/parks/driver-profiles/transactions',$query);
                Log::error('info '.$response->body().$order->id);
                return response("<pg_status>ok</pg_status>",200);

            }catch (\Exception $e){
                Log::error('Error '.$e.'\n'.$request->getContent());
            }
        }
        return false;
    }
    public function withdrawDelivery(Request $request)
    {
        if (!$request->has('pg_xml')){
            return false;
        }

        Log::error('PG_XML '.$request->pg_xml);

        $pg_xml = $request->pg_xml;
        $xml = new \SimpleXMLElement($pg_xml);
        if ($xml->pg_status == 'ok'){
            try{
                $order = Order::find($xml->pg_order_id);

                if($order['payment_status_id'] == 4){
                    Log::error('PG_XML '.$request->pg_xml);
                    return response([],200);
                }
                $order['payment_status_id'] = 4;
                $order->save();
                $user = User::find($order['user_id']);
                $user['balance'] -= ($order->price >=5000 ? $order->price : $order['price']+300);
                $user->save();
                $park = new \stdClass();
                $park->id = '1bec42866c27423fb612b05e18f0c666' ;
                $query = new \stdClass();
                $query->amount ="-".($order->price >= 5000 ? $order->price : $order->price+300)."";
                $query->category_id = "partner_service_manual_8";
                $query->description = "Вывод номер ".$order->id. " .Номер :".$user->phone;
                $query->driver_profile_id =$user->driver_id;
                $query->park_id =  '1bec42866c27423fb612b05e18f0c666' ;
                $query = (array) $query;
                $response = Http::withHeaders([
                    'Accept' => 'application/json',
                    'X-Client-ID' => 'taxi/park/1bec42866c27423fb612b05e18f0c666',
                    'X-Api-Key' => 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX',
                    'Content-Type' => 'application/json',
                    'Accept-Language' => 'ru',
                    'X-Idempotency-Token' => (string) Str::uuid()
                ])->post('https://fleet-api.taxi.yandex.net/v2/parks/driver-profiles/transactions',$query);
                Log::error('info '.$response->body().$order->id);
                return response("<pg_status>ok</pg_status>",200);

            }catch (\Exception $e){
                Log::error('Error '.$e.'\n'.$request->getContent());
            }
        }
        return false;
    }

    public function paymentStatus(Request $request)
    {
        Log::error('request '.$request->getContent());
        try{
            if ($request->pg_result == 1){
                $order = Order::find($request->pg_order_id);
                $order['payment_status_id'] = 3;
                $order->save();
                $user = User::find($order['user_id']);
                $user['balance'] += $order['price'];
                $user->save();
                $query = new \stdClass();
                $query->amount =(string) $order->price;
                $query->category_id = "partner_service_manual_10";
                $query->description = "Пополнение номер ".$order->id. " .Номер :".$user->phone;
                $query->driver_profile_id =$user->driver_id;
                $query->park_id = $request->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
                $query = (array) $query;
                $response = Http::withHeaders([
                    'Accept' => 'application/json',
                    'X-Client-ID' => $request->delivery ? 'taxi/park/1bec42866c27423fb612b05e18f0c666' : 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                    'X-Api-Key' => $request->delivery ? 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX' : 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                    'Content-Type' => 'application/json',
                    'Accept-Language' => 'ru',
                    'X-Idempotency-Token' => (string) Str::uuid()
                ])->post('https://fleet-api.taxi.yandex.net/v2/parks/driver-profiles/transactions',$query);
                Log::error('response '.$response->body().$order->id);
                if ($response->status() == 200){
                    return true;
                }
                return true;
            }

        }catch (\Exception $exception){
            Log::error('Error '.$exception.'\n'.$request->getContent());
        }
        return false;
    }
}
