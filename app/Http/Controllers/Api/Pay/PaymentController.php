<?php

namespace App\Http\Controllers\Api\Pay;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\UserCard;
use Illuminate\Http\Request;
use App\Paybox\Paybox;
use Psy\Util\Str;

class PaymentController extends Controller
{
    //
    public function pay(Request $request)
    {
        return response(['message' => 'Пополнение баланса недоступна'],404);

        $valData = $request->validate([
            'price' => 'required',
            'user_card_id' => 'required'
        ]);
        $user = $request->user();
        $order = Order::create([
           'payment_type_id' => 1,
           'user_id' => $user->id,
           'user_card_id' => $valData['user_card_id'],
           'payment_status_id' => 1,
           'price' => $valData['price'],
        ]);
        $user_card_id = UserCard::find($valData['user_card_id']);
        $pay = new Paybox();
        if (!$user_card_id)
            return response(['message' => 'Такой карты нет'],404);
        $pay->setQuery([
            'pg_amount' => $valData['price'],
            'pg_order_id' =>(string) $order->id,
            'pg_user_id' =>(string) $user->id,
            'pg_card_id' => $user_card_id->card_id,
            'pg_description' => 'Заказ номер '.$order->id,
            'pg_result_url' => route('paymentStatus'),
            'pg_salt' => \Illuminate\Support\Str::random(10),
            'pg_success_url' => route('login'),
            'pg_failure_url' => route('login'),
            'delivery' => $request->delivery

        ]);
//        $result =new  \SimpleXMLElement($pay->pay());

        return response()->json(['payment' => $pay->pay()],201);


    }
}
