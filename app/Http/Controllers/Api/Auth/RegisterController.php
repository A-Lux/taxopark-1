<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\SmsCode;
use App\Models\User;
use App\Traits\RequestConstructor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    //
    protected $busyPhone = '+77017170693';
    public function register(Request $request)
    {
//        if ($request->has('phone')){
//            $user = User::where('phone',$request->phone)->first();
//            if ($user){
//                return response()->json(['message' => 'Этот номер телефона занят'],422);
//            }
//        }
        $exception = 0;
        $i = 0;
        $delivery = $request['delivery'] ? 'Доставка':'Таксопарк' ;
        $response = Http::get(
            'https://yandex.bitrix24.kz/rest/1/pm925xde4xwwc0fu/crm.lead.add.json?FIELDS[TITLE]='
            .'Заявка с мобильного приложения'
            .'&FIELDS[NAME]='.$request['name']
            .'&FIELDS[LAST_NAME]='.$request['surname']
            .'&FIELDS[PHONE][0][VALUE_TYPE]='.$request['phone']
            .'&FIELDS[COMMENTS]= <b>Имя :</b>'.$request['name'].'<b> Фамилия:</b>'.$request['surname'].'<b> Телефон:</b>'.$request['phone'].'<b> Парк: </b> '.$delivery
        );
        return response()->json(['message' => 'Ваша заявка была создана в скором времени ее одобрят '],200);

        while ($exception < 1){



            $validData = $request->validate([
                'name' => 'required',
                'surname' => 'required',
                'phone' => 'required|unique:users',
                'delivery' => 'nullable'
            ]);
            $park = new \stdClass();
            $park->id = $request->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
            $phone = new \stdClass();
            $phone->phones = [
                $validData['phone']
            ];
            $park->driver_profile = $phone;
            $query = new \stdClass();
            $query->park = $park;

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'X-Client-ID' => $request->delivery ? 'taxi/park/1bec42866c27423fb612b05e18f0c666' : 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                'X-Api-Key' => $request->delivery ? 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX' : 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                'Content-Type' => 'application/json',
                'Accept-Language' => 'ru'
            ])->post('https://fleet-api.taxi.yandex.net/v1/parks/driver-profiles/list', [
                'query' => $query,
                'offset' => 1000 * $i
            ]);
            $response = json_decode($response);

            $exception = 0;
            if (count($response->driver_profiles) < 1){
                $exception = 2;
                break;
            }

            foreach ($response->driver_profiles as $profile){
                if(in_array($validData['phone'],$profile->driver_profile->phones)) {
                    $exception = 2;
                    break;
                }
            }

            $i +=1;
        }






        if ($exception == 2){
            $user = User::create([
               'role_id' => 3,
               'name' => $validData['name'],
               'surname' => $validData['surname'],
               'phone' => $validData['phone'],
               'password' => Hash::make(Str::random(10)),
            ]);
            $sms_code = mt_rand(1111,9999);
            $code = SmsCode::create([
               'user_id' => $user->id,
               'code' => $sms_code,
            ]);
            $message = 'https://ytaxi.kz/ ваш смс код: '.$code->code;
            $sms_code_array = [
                'login' => 'taxopark_one',
                'psw' => 'vYgjk4r',
                'phones' => $user['phone'],
                'mes' => $message
            ];

            try{
                $response = Http::get('http://smsc.kz/sys/send.php?login='.$sms_code_array['login'].'&psw='.$sms_code_array['psw'].'&phones='.$sms_code_array['phones'].'&mes='.$sms_code_array['mes']);

            }catch (Exception $e){
                return  response(['message' => 'Ошибка '.$e],500);
            }
            return response()->json(['sms_code' => $code, 'message' => 'Мы прислали вам смс код'],201);
        }

        return response()->json(['message' => 'Этот номер телефона занят'],422);
    }


    public function checkCode(Request $request)
    {
        $validData = $request->validate([
           'user_id' => 'required',
           'code' => 'required'
        ]);


        $sms_code = SmsCode::where('user_id',$validData['user_id'])->first();
        if (!$sms_code){
            return response()->json(['message' => 'Такого кода нет'],404);

        }
        if ($sms_code['code'] == $validData['code']){
            $user = User::find($validData['user_id']);

            return response()->json(['message' => 'Ваша заявка была создана в скором времени ее одобрят '],201);

        }

    }


}
