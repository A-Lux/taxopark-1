<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\SmsCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    //

    public function registerAllUsers()
    {
        $exception = 0;
        $i = 0;
        while ($exception < 1) {
            $park = new \stdClass();
            $park->id = '294a77124cdf4c73b462609a2c3be683';

            $query = new \stdClass();
            $query->park = $park;

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'X-Client-ID' => 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                'X-Api-Key' => 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                'Content-Type' => 'application/json',
                'Accept-Language' => 'ru'
            ])->post('https://fleet-api.taxi.yandex.net/v1/parks/driver-profiles/list', [
                'query' => $query,
                'offset' => 1000 * $i
            ]);
            $response = json_decode($response);


//            return response(['message' => $response]);

//            if (count($response->driver_profiles) > 1){
            return response()->json(['message' => $response], 404);

//            }
            foreach ($response->driver_profiles as $profile) {

                $yandex_profile = $profile->driver_profile;
                $yandex_account = $profile->accounts[0];

                $user = User::where('phone', $yandex_profile->phones[0])->first();
                Log::error($user);

                if (!$user) {
                    User::create([
                        'name' => $yandex_profile->first_name,
                        'surname' => $yandex_profile->last_name,
                        'role_id' => 2,
                        'email' => null,
                        'password' => Hash::make(Str::random(10)),
                        'phone' => $yandex_profile->phones[0],
                        'balance' => $yandex_account->balance,
                        'driver_id' => $yandex_profile->id
                    ]);
                }
            }

            $i += 1;
        }
    }

    public function login(Request $request)
    {
        $validData = $request->validate([
            'phone' => 'required',
            'delivery' => 'nullable'
        ]);
        $i = 0;
        $exception = 0;
        Log::error('delivery '.$request->delivery);
        while ($exception < 1) {
            $park = new \stdClass();
            $park->id = $request->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
            $phone = new \stdClass();
            $phone->phones = [
                $validData['phone']
            ];
            $park->driver_profile = $phone;
            $query = new \stdClass();
            $query->park = $park;

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'X-Client-ID' => $request->delivery ? 'taxi/park/1bec42866c27423fb612b05e18f0c666' : 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                'X-Api-Key' => $request->delivery ? 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX' : 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                'Content-Type' => 'application/json',
                'Accept-Language' => 'ru'
            ])->post('https://fleet-api.taxi.yandex.net/v1/parks/driver-profiles/list', [
                'query' => $query,
                'offset' => 1000 * $i
            ]);
            $response = json_decode($response);
            $yandex_profile = '';
//            return response(['message' => $response]);

            if (count($response->driver_profiles) < 1) {
                return response()->json(['message' => 'Пользователь с таким телефоном не найден'], 404);

            }
            foreach ($response->driver_profiles as $profile) {

                if (in_array($validData['phone'], $profile->driver_profile->phones)) {
                    $yandex_profile = $profile->driver_profile;
                    $yandex_account = $profile->accounts[0];
                    if ( $yandex_profile->work_status == 'fired'){
                        return response()->json(['message' => 'Пользователь с таким телефоном не найден'], 404);
                    }
                    if ($yandex_profile->work_rule_id == '4045e1cdbdb14ee1975c405480ddb332'
                        || $yandex_profile->work_rule_id == 'ee222adb75bc4d77a38682bc901d84d9'
                        || $yandex_profile->work_rule_id == '7350f01b3f3048cdb23606b366213650'
                        || $yandex_profile->work_rule_id == '603c7c520b0c4667befc9447d2cf4b9f'
                    ) {
                        return response(['message' => 'Ваш номер телефона не соответствует условиям'], 429);
                    }

                    $exception = 1;
                    break;
                }
            }

            $i += 1;
        }

        if ($exception == 1) {
            $user = User::where('phone', $validData['phone'])->first();
            if (!$user) {
                $user = User::create([
                    'name' => $yandex_profile->first_name,
                    'surname' => $yandex_profile->last_name,
                    'role_id' => 2,
                    'email' => null,
                    'password' => Hash::make(Str::random(10)),
                    'phone' => $yandex_profile->phones[0],
                    'balance' => $yandex_account->balance,
                    'driver_id' => $yandex_profile->id
                ]);


            }
            $user->update(['balance' => $yandex_account->balance]);

            $sms_code = SmsCode::where('user_id', $user['id'])->first();

            if (!$sms_code) {
                $sms_code = SmsCode::create(['user_id' => $user->id, 'code' => mt_rand(1111, 9999)]);

            }


            $sms_code = SmsCode::where('user_id', $user['id'])->first();
            if (!$sms_code) {
                $sms_code = SmsCode::create(['user_id' => $user->id, 'code' => mt_rand(1111, 9999)]);
                $message = 'https://ytaxi.kz/ ваш смс код: ' . $sms_code->code;
                $sms_code_array = [
                    'login' => 'taxopark_one',
                    'psw' => '9QGSzTN555pdFAK',
                    'phones' => $user['phone'],
                    'mes' => $message
                ];
                try {
                    $response = Http::get('http://smsc.kz/sys/send.php?login=' . $sms_code_array['login'] . '&psw=' . $sms_code_array['psw'] . '&phones=' . $sms_code_array['phones'] . '&mes=' . $sms_code_array['mes']);

                } catch (Exception $e) {
                    return response(['message' => 'Ошибка ' . $e], 500);
                }
                return response()->json(['code' => $sms_code], 200);
            }

            $sms_code->update(['code' => mt_rand(1111, 9999)]);
            $message = route('login') . ' ваш смс код: ' . $sms_code->code;
            $sms_code_array = [
                'login' => 'taxopark_one',
                'psw' => '9QGSzTN555pdFAK',
                'phones' => $user['phone'],
                'mes' => $message
            ];
            try {
                $response = Http::get('http://smsc.kz/sys/send.php?login=' . $sms_code_array['login'] . '&psw=' . $sms_code_array['psw'] . '&phones=' . $sms_code_array['phones'] . '&mes=' . $sms_code_array['mes']);
            } catch (Exception $e) {
                return response(['message' => 'Ошибка ' . $e], 500);
            }
            return response()->json(['code' => $sms_code], 200);
        }
        return response()->json(['message' => 'Пользователь с таким телефоном не найден'], 404);
    }

    public function logout(Request $request)
    {
        dd(1);
        try {
            $request->user()->token()->revoke();

            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error ' . $e]);

        }

    }

    public function checkCode(Request $request)
    {
        $validData = $request->validate([
            'user_id' => 'required',
            'code' => 'required',
        ]);


        $code = SmsCode::where('user_id', $validData['user_id'])->first();
        if ($code['code'] == $validData['code']) {
            $user = User::find($validData['user_id']);
            $user['avatar'] = asset('storage/' . str_replace('public/', '', $user->avatar));
            $token = $user->createtoken('auth_token');
            return response(['user' => $user, 'token' => $token->accessToken, 'expires_at' => Carbon::parse(
                $token->token->expires_at
            )->toDateTimeString()
            ], 200);
        }
        return response(['message' => 'Не верный код'], 422);
    }

    public function getUserData(Request $request)
    {
        $i = 0;
        $exception = 0;
        while ($exception < 1) {

            $user = $request->user();

            $park = new \stdClass();
            $park->id = $request->delivery ? '1bec42866c27423fb612b05e18f0c666' : '294a77124cdf4c73b462609a2c3be683';
            $query = new \stdClass();
            $query->park = $park;
            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'X-Client-ID' => $request->delivery ? 'taxi/park/1bec42866c27423fb612b05e18f0c666' : 'taxi/park/294a77124cdf4c73b462609a2c3be683',
                'X-Api-Key' => $request->delivery ? 'hKZUKtdmwhbQIbPyHwpsgtrCjGpxywEmIrX' : 'bVMAZaE+CKhoHygcIbpMCQcyas/mZzddmSI',
                'Content-Type' => 'application/json',
                'Accept-Language' => 'ru'
            ])->post('https://fleet-api.taxi.yandex.net/v1/parks/driver-profiles/list', [
                'query' => $query,
                'offset' => 1000 * $i
            ]);
            $response = json_decode($response);
            $yandex_profile = '';

            $yandex_account = [];


            foreach ($response->driver_profiles as $profile) {

                if (in_array($user['phone'], $profile->driver_profile->phones)) {
                    $yandex_profile = $profile->driver_profile;
                    $yandex_account = $profile->accounts[0];
                    $exception = 1;
                    break;
                }
            }
            $i += 1;
        }
        if ($exception == 1) {
            $user->update(
                [
                    'balance' => round($yandex_account->balance),
                    'driver_id' => $yandex_profile->id,
                    'name' => $yandex_profile->first_name,
                    'surname' => $yandex_profile->last_name,

                ]
            );

            $user['avatar'] = asset('storage/' . str_replace('public/', '', $user->avatar));

        }
        return response(['user' => $user], 200);
    }

    public function updateAvatar(Request $request)
    {
        $valData = $request->validate([
            'avatar' => 'required',
        ]);

        $user = $request->user();
        $user->update([
            'avatar' => str_replace('public/', '', $request->file('avatar')->store('public/users'))

        ]);

        return response([], 201);
    }

}
