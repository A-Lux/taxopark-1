<?php

namespace App\Http\Controllers\Api\Card;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\UserCard;
use App\Paybox\Paybox;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CardController extends Controller
{
    //

    public function linkCard(Request $request)
    {
        $user = $request->user();
        if (!$user){
            return response(['message' => 'Такого пользователя нет'],404);
        }
        $query = [
            'pg_user_id' => $user['id'],
            'pg_post_link' => route('acceptCard'),
            'pg_back_link' => route('login'),
            'pg_salt' => Str::random(10),
        ];

        $payment = new Paybox();
        $payment->setQuery($query);
        $response = $payment->addCard();
        $xml = (array) new \SimpleXMLElement($response->body());



        return response(['response' => $xml],200);
    }

    public function acceptCard(Request $request)
    {

        if ($request['pg_xml']){
            Log::error(print_r($request['pg_xml']));
            $xml = $request->pg_xml;
            $payment = new Paybox();
            $save_card = $payment->saveCard($xml);
            if ($save_card){
                return response([$save_card],200);
            }
        }else{
            return response([],400);
        }
    }

    public function getCards(Request $request)
    {
        $user = $request->user();
        if (!$user){
            return response(['message' => 'Такой пользователь не найден'],404);
        }

        $cards = UserCard::where('user_id',$user['id'])->get();
        return response(['cards' => $cards],200);
    }

    public function deleteCard(Request $request)
    {
        $valData = $request->validate([
           'card_id' =>  'required',
        ]);
        $user = $request->user();
        $card = UserCard::find($valData['card_id']);

        $orders = Order::where('user_card_id',$valData['card_id'])->get();
        foreach ($orders as $order){
            $order['user_card_id'] = null;
            $order->save();
        }
        $card->delete();
        return response([],200);

    }
}
