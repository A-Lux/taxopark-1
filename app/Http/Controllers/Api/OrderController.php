<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public function orders(Request $request)
    {
        $valData = $request->validate([

            'withdraw' => 'nullable'
        ]);
        $user = $request->user();

        $orders = Order::with(['card','status','type'])
            ->where('user_id',$user->id)
            ->where('payment_status_id',3)

            ->where(function($query){
                $query->where('payment_type_id',1);

            })
            ->orderBy('id','desc')
            ->paginate(10);
        if ($request->withdraw == 1){
            $orders = Order::with(['card','status','type'])
                ->where('user_id',$user->id)
                ->where('payment_status_id',4)
                ->where(function($query){
                    $query->where('payment_type_id',2);
                    $query->orWhere('payment_type_id',3);
                })
                ->orderBy('id','desc')
                ->paginate(10);
        }
        return response(['orders' => $orders],200);


    }
}
