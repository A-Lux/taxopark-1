<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
      'payment_type_id',
      'user_id',
      'user_card_id',
      'payment_status_id',
      'price',
      'currency'
    ];

    public function type()
    {
        return $this->belongsTo(PaymentType::class,'payment_type_id','id');
    }

    public function status()
    {
        return $this->belongsTo(PaymentStatus::class,'payment_status_id','id');
    }

    public function card()
    {
        return $this->belongsTo(UserCard::class,'user_card_id','id');
    }
}
